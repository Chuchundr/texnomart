from django.urls import path
from . import views


app_name = 'goods'

urlpatterns = [
    path('goods/detail/<uuid:pk>', views.GoodDetailView.as_view(), name='good-detail')
]
