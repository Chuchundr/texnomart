"""
Module admin
"""
from django.contrib import admin
from goods import models as goods_model


@admin.register(goods_model.Categories)
class Categories(admin.ModelAdmin):
    """
    Класс для Родительской Катигорий товара
    """
    model = goods_model.Categories
    list_display = ['name']
    search_fields = ['name']
    list_filter = ['name']


@admin.register(goods_model.Category)
class Category(admin.ModelAdmin):
    """
    Класс для Катигорий товара
    """
    model = goods_model.Category
    list_display = ['name']
    search_fields = ['name']
    list_filter = ['name']


@admin.register(goods_model.Specification)
class Specification(admin.ModelAdmin):
    """
    Класс для Спецификации товара
    используется Inline
    """
    model = goods_model.Specification
    list_display = ['name', 'value']
    autocomplete_fields = ['good']
    search_fields = ['name', 'value']


@admin.register(goods_model.SpecificationName)
class SpecificationName(admin.ModelAdmin):
    """
    Класс для Спецификации товара
    используется Inline
    """
    model = goods_model.SpecificationName
    list_display = ['name']
    search_fields = ['name']


@admin.register(goods_model.SpecificationValue)
class SpecificationValue(admin.ModelAdmin):
    """
    Класс для Спецификации товара
    используется Inline
    """
    model = goods_model.SpecificationValue
    list_display = ['value']
    search_fields = ['value']


@admin.register(goods_model.SpecificationNameBool)
class SpecificationNameBool(admin.ModelAdmin):
    """
    Класс для Спецификации товара
    используется Inline
    """
    model = goods_model.SpecificationNameBool
    list_display = ['name']
    search_fields = ['name']


class SpecificationInline(admin.TabularInline):
    """
    Класс для Спецификации товара
    используется Inline
    """
    model = goods_model.Specification
    list_display = ['name.name', 'value.value']
    autocomplete_fields = ['good', 'name', 'value']
    search_fields = ['name.name', 'value.value']
    extra = 1


class SpecificationBoolInline(admin.TabularInline):
    """
    Класс для Спецификации товара
    используется Inline
    """
    model = goods_model.SpecificationBool
    list_display = ['name.name', 'value']
    autocomplete_fields = ['good', 'name']
    search_fields = ['name.name', 'value']
    extra = 1


@admin.register(goods_model.GoodName)
class GoodName(admin.ModelAdmin):
    """
    Класс для Имени Товара
    """
    list_display = ['name']
    search_fields = ['name']
    list_filter = ['name']


@admin.register(goods_model.Good)
class Good(admin.ModelAdmin):
    """
    Класс для Товара
    """
    autocomplete_fields = ['name', 'category', 'store']
    inlines = [SpecificationInline, SpecificationBoolInline]
    list_display = ['name', 'model', 'has_prepayment', 'category', 'in_stock', 'slug']
    search_fields = ['name.name', 'model']
    list_filter = ['name', 'spec', 'in_stock', 'category', 'store']
