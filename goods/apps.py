"""
Module app
"""
from django.apps import AppConfig


class GoodsConfig(AppConfig):
    """
    Class with application name
    """
    name = 'goods'
    verbose_name = 'Товары'

    def ready(self):
        import goods.signals
