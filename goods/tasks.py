"""
package: goods
description: tasks
"""
from __future__ import absolute_import, unicode_literals
import logging
from django.db import IntegrityError, OperationalError
from kombu import exceptions as kombu_exceptions
from aiohttp.client_exceptions import ClientConnectorError, ClientOSError
from project.celery import app
from goods import managers, models


LOGGER = logging.getLogger(__name__)


@app.task(bind=True)
def update_or_create_categories(self, data):
    try:
        manager = managers.CategoriesManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_category(self, data):
    try:
        manager = managers.CategoryManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_good_name(self, data):
    try:
        manager = managers.GoodNameManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_good(self, data):
    try:
        manager = managers.GoodManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_specification_name(self, data):
    try:
        manager = managers.SpecificationNameManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_specification_name_bool(self, data):
    try:
        manager = managers.SpecificationNameBoolManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_specification_value(self, data):
    try:
        manager = managers.SpecificationValueManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_specification(self, data):
    try:
        manager = managers.SpecificationManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_specification_bool(self, data):
    try:
        manager = managers.SpecificationBoolManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_categories(self):
    try:
        manager = managers.CategoriesManager()
        for item in manager.get_list():
            models.Categories.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_category(self):
    try:
        manager = managers.CategoryManager()
        for item in manager.get_list():
            models.Category.objects.update_or_create(
                parent_id=item.pop('parent'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_good_name(self):
    try:
        manager = managers.GoodNameManager()
        for item in manager.get_list():
            models.GoodName.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_good(self):
    try:
        manager = managers.GoodManager()
        for item in manager.get_list():
            models.Good.objects.update_or_create(
                store_id=item.pop('store'),
                name_id=item.pop('name'),
                category_id=item.pop('category'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_specification_name(self):
    try:
        manager = managers.SpecificationNameManager()
        for item in manager.get_list():
            models.SpecificationName.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_specification_name_bool(self):
    try:
        manager = managers.SpecificationNameBoolManager()
        for item in manager.get_list():
            models.SpecificationNameBool.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_specification_value(self):
    try:
        manager = managers.SpecificationValueManager()
        for item in manager.get_list():
            models.SpecificationValue.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_specification(self):
    try:
        manager = managers.SpecificationManager()
        for item in manager.get_list():
            models.Specification.objects.update_or_create(
                name_id=item.pop('name'),
                value_id=item.pop('value'),
                good_id=item.pop('good'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_specification_bool(self):
    try:
        manager = managers.SpecificationBoolManager()
        for item in manager.get_list():
            models.SpecificationBool.objects.update_or_create(
                name_id=item.pop('name'),
                good_id=item.pop('good'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_categories(self, data):
    try:
        manager = managers.CategoriesManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_category(self, data):
    try:
        manager = managers.CategoryManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_good_name(self, data):
    try:
        manager = managers.GoodNameManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_good(self, data):
    try:
        manager = managers.GoodManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_specification_name(self, data):
    try:
        manager = managers.SpecificationNameManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_specification_name_bool(self, data):
    try:
        manager = managers.SpecificationNameBoolManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_specification_value(self, data):
    try:
        manager = managers.SpecificationValueManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_specification(self, data):
    try:
        manager = managers.SpecificationManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_specification_bool(self, data):
    try:
        manager = managers.SpecificationBoolManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)
