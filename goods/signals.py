"""
package: goods
description: signals
"""
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.conf import settings
from goods import models, tasks


@receiver(post_save, sender=models.Categories)
def update_or_create_categories_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_categories.delay(instance.to_dict())


@receiver(post_save, sender=models.Category)
def update_or_create_category_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_category.delay(instance.to_dict())


@receiver(post_save, sender=models.GoodName)
def update_or_create_good_name_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_good_name.delay(instance.to_dict())


@receiver(post_save, sender=models.Good)
def update_or_create_good_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_good.delay(instance.to_dict())


@receiver(post_save, sender=models.SpecificationName)
def update_or_create_specification_name_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_specification_name.delay(instance.to_dict())


@receiver(post_save, sender=models.SpecificationNameBool)
def update_or_create_specification_name_bool_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_specification_name_bool.delay(instance.to_dict())


@receiver(post_save, sender=models.SpecificationValue)
def update_or_create_specification_value_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_specification_value.delay(instance.to_dict())


@receiver(post_save, sender=models.Specification)
def update_or_create_specification_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_specification.delay(instance.to_dict())


@receiver(post_save, sender=models.SpecificationBool)
def update_or_create_specification_bool_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_specification_bool.delay(instance.to_dict())


@receiver(post_delete, sender=models.Categories)
def delete_categories_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_categories.delay(instance.to_dict())


@receiver(post_delete, sender=models.Category)
def delete_category_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_category.delay(instance.to_dict())


@receiver(post_delete, sender=models.GoodName)
def delete_good_name_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_good_name.delay(instance.to_dict())


@receiver(post_delete, sender=models.Good)
def delete_good_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_good.delay(instance.to_dict())


@receiver(post_delete, sender=models.SpecificationName)
def delete_specification_name_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_specification_name.delay(instance.to_dict())


@receiver(post_delete, sender=models.SpecificationNameBool)
def delete_specification_name_bool_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_specification_name_bool.delay(instance.to_dict())


@receiver(post_delete, sender=models.SpecificationValue)
def delete_specification_value_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_specification_value.delay(instance.to_dict())


@receiver(post_delete, sender=models.Specification)
def delete_specification_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_specification.delay(instance.to_dict())


@receiver(post_delete, sender=models.SpecificationBool)
def delete_specification_bool_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_specification_bool.delay(instance.to_dict())
