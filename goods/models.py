"""
Module models
"""
from uuid import uuid4
from django.db import models
from django.urls import reverse
from pytils.translit import slugify
from stores import models as store_models


class Categories(models.Model):
    """
    Model Categories
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    name = models.CharField(max_length=100, unique=True, verbose_name="Название Родительской категории")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    class Meta:
        verbose_name = 'Родительскую Категорию'
        verbose_name_plural = 'Родительская Категория'

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'name': self.name,
        }


class Category(models.Model):
    """
    Model Category
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    parent = models.ForeignKey(
        Categories,
        null=True,
        blank=True,
        on_delete=models.CASCADE,
        verbose_name="Родительская категория"
    )
    name = models.CharField(max_length=100, unique=True, verbose_name="Название категории")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'parent': str(self.parent.uuid),
            'name': self.name
        }

    class Meta:
        verbose_name = 'Категорию'
        verbose_name_plural = 'Категория'


class GoodName(models.Model):
    """
    Model GoodName
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    name = models.CharField(max_length=100, verbose_name="Название Товара")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'name': self.name
        }

    class Meta:
        verbose_name = 'Название Товара'
        verbose_name_plural = 'Название Товара'


class Good(models.Model):
    """
    Model Good
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    store = models.ForeignKey(store_models.Store, blank=True, null=True, on_delete=models.CASCADE, verbose_name="Склад")
    name = models.ForeignKey(GoodName, on_delete=models.CASCADE, verbose_name="Название Товара")
    photo = models.ImageField(blank=True, upload_to='good-photo', default='girl.jpg', verbose_name='Фото товара')
    model = models.CharField(max_length=200, verbose_name="Модель")
    total_price = models.DecimalField(
        max_digits=50,
        decimal_places=2,
        verbose_name='Общая цена'
    )
    credit_time = models.PositiveIntegerField(
        default=12,
        verbose_name='Время кредита'
    )
    has_prepayment = models.BooleanField(
        default=True, verbose_name="Есть предоплата"
    )
    category = models.ForeignKey(
        Category,
        on_delete=models.CASCADE,
        verbose_name="Категория Товара"
    )
    in_stock = models.BooleanField(
        default=False,
        verbose_name="В наличии"
    )
    slug = models.SlugField(
        max_length=250,
        editable=False,
        db_index=True,
        blank=True,
        verbose_name='ЧПУ'
    )
    description = models.TextField(
        max_length=500,
        null=True,
        blank=True,
        verbose_name="Описание товара"
    )
    created = models.DateTimeField(
        auto_now_add=True,
        editable=False,
        verbose_name='Дата добавления'
    )
    edited = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name='Дата редактирования'
    )

    def get_absolute_url(self):
        """
        Function returns url
        """
        return reverse('goods:good-detail', kwargs={'slug': self.slug})

    def __str__(self):
        return f'{self.name.name}, {self.model}'

    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
        self.slug = slugify(f'{self.name.name}')
        super(Good, self).save(*args, **kwargs)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'store': str(self.store.uuid),
            'name': str(self.name.uuid),
            'model': self.model,
            'total_price': str(self.total_price),
            'credit_time': self.credit_time,
            'has_prepayment': self.has_prepayment,
            'category': str(self.category.uuid),
            'in_stock': self.in_stock,
            'description': self.description
        }

    class Meta:
        unique_together = ['name', 'model', 'category']
        verbose_name = 'Партия товара'
        verbose_name_plural = 'Партии товара'


class SpecificationName(models.Model):
    """
    Model SpecificationName
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    name = models.CharField(max_length=250, unique=True, blank=True, verbose_name="Название характеристики")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'name': self.name
        }

    class Meta:
        verbose_name = 'Название Спецификации для Текстовых'
        verbose_name_plural = 'Название Спецификации для Текстовых'


class SpecificationNameBool(models.Model):
    """
    Model SpecificationNameBool
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    name = models.CharField(max_length=250, unique=True, blank=True, null=True, verbose_name="Название характеристики")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'name': self.name
        }

    class Meta:
        verbose_name = 'Название Спецификации для Булево'
        verbose_name_plural = 'Название Спецификации для Булево'


class SpecificationValue(models.Model):
    """
    Model SpecificationValue
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    value = models.CharField(max_length=250, unique=True, blank=True, verbose_name="Значение характеристики")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.value}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'value': self.value
        }

    class Meta:
        verbose_name = 'Значение Спецификации'
        verbose_name_plural = 'Значение Спецификации'


class Specification(models.Model):
    """
    Model Specification
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    name = models.ForeignKey(SpecificationName, on_delete=models.CASCADE, verbose_name="Название характеристики")
    value = models.ForeignKey(SpecificationValue, on_delete=models.CASCADE, verbose_name="Значение характеристики")
    good = models.ForeignKey(Good, on_delete=models.CASCADE, related_name='spec', verbose_name="Название товара")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name.name}, {self.value.value}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'name': str(self.name.uuid),
            'value': str(self.value.uuid),
            'good': str(self.good.uuid)
        }

    class Meta:
        verbose_name = 'Спецификацию'
        verbose_name_plural = 'Спецификация'


class SpecificationBool(models.Model):
    """
    Model SpecificationBool
    """
    CHOICES = (
        ('Нет', 'Нет'),
        ('Есть', 'Есть'),
        )
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    name = models.ForeignKey(SpecificationNameBool, on_delete=models.CASCADE, blank=True, verbose_name="Название характеристики")
    value = models.CharField(max_length=250, default=False, blank=True, null=True, choices=CHOICES, verbose_name="Значение характеристики")
    good = models.ForeignKey(Good, on_delete=models.CASCADE, related_name='spec_bool', verbose_name="Название товара")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return f'{self.name.name}, {self.value}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'name': str(self.name.uuid),
            'value': self.value,
            'good': str(self.good.uuid)
        }

    class Meta:
        verbose_name = 'Спецификацию для Булево'
        verbose_name_plural = 'Спецификация для Булево'
