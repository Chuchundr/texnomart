"""
package: goods
Module views
"""
from django.views import generic
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.conf import settings
from goods import models
from basket import forms as basket_forms


class GoodDetailView(generic.DetailView):
    '''
    detail view class for good
    '''
    model = models.Good
    template_name = 'goods/good_detail.html'

    def get_context_data(self, *args, **kwargs): # pylint: disable=arguments-differ
        context = super(GoodDetailView, self).get_context_data(*args, **kwargs)
        context['basket_form'] = basket_forms.BasketForm
        return context
