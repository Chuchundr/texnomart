from libraries.base import BaseManager


class CategoriesManager(BaseManager):
    """
    manager for categories
    """
    URI = '/api/goods/categories/'


class CategoryManager(BaseManager):
    """
    manager for category
    """
    URI = '/api/goods/category/'


class GoodNameManager(BaseManager):
    """
    manager for good names
    """
    URI = '/api/goods/good_name/'


class GoodManager(BaseManager):
    """
    manager for goods
    """
    URI = '/api/goods/good/'


class SpecificationNameManager(BaseManager):
    """
    manager for specification names
    """
    URI = '/api/goods/specification_name/'


class SpecificationNameBoolManager(BaseManager):
    """
    manager for specification name bool
    """
    URI = '/api/goods/specification_name_bool/'


class SpecificationValueManager(BaseManager):
    """
    manager for specification value
    """
    URI = '/api/goods/specification_value/'
    
    
class SpecificationManager(BaseManager):
    """
    manager for specifications
    """
    URI = '/api/goods/specification/'


class SpecificationBoolManager(BaseManager):
    """
    manager for specification bool
    """
    URI = '/api/goods/specification_bool/'
