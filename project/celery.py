"""
celery config
"""
from __future__ import absolute_import, unicode_literals

import os

from celery import Celery
from celery.schedules import crontab

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'project.settings')

app = Celery()

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()

app.conf.beat_schedule = {
    'Address Country Periodic update': {
        'task': 'address.tasks.get_country', 'schedule': crontab(minute='*/1'),
    },
    'Address Region periodic update': {
        'task': 'address.tasks.get_region', 'schedule': crontab(minute='*/1'),
    },
    'Address City periodic update': {
        'task': 'address.tasks.get_city', 'schedule': crontab(minute='*/1'),
    },
    'Address Area periodic update': {
        'task': 'address.tasks.get_area', 'schedule': crontab(minute='*/1'),
    },
    'Address Street periodic update': {
        'task': 'address.tasks.get_street', 'schedule': crontab(minute='*/1'),
    },
    'Address Address periodic update': {
        'task': 'address.tasks.get_address', 'schedule': crontab(minute='*/1')
    },
    'Clients Contacts periodic update': {
        'task': 'clients.tasks.get_contacts', 'schedule': crontab(minute='*/1'),
    },
    'Clients Personal Data periodic update': {
        'task': 'clients.tasks.get_personal_data', 'schedule': crontab(minute='*/1'),
    },
    'Goods Categories periodic update': {
        'task': 'goods.tasks.get_categories', 'schedule': crontab(minute='*/1'),
    },
    'Goods Category periodic update': {
        'task': 'goods.tasks.get_category', 'schedule': crontab(minute='*/1'),
    },
    'Goods Good Name periodic update': {
        'task': 'goods.tasks.get_good_name', 'schedule': crontab(minute='*/1'),
    },
    'Goods Good periodic update': {
        'task': 'goods.tasks.get_good', 'schedule': crontab(minute='*/1'),
    },
    'Goods Specification Name periodic update': {
        'task': 'goods.tasks.get_specification_name', 'schedule': crontab(minute='*/1'),
    },
    'Goods Specification Name Bool periodic update': {
        'task': 'goods.tasks.get_specification_name_bool', 'schedule': crontab(minute='*/1'),
    },
    'Goods Specification Value periodic update': {
        'task': 'goods.tasks.get_specification_value', 'schedule': crontab(minute='*/1'),
    },
    'Goods Specification periodic update': {
        'task': 'goods.tasks.get_specification', 'schedule': crontab(minute='*/1'),
    },
    'Goods Speecificatio Bool periodic update': {
        'task': 'goods.tasks.get_specification_bool', 'schedule': crontab(minute='*/1'),
    },
    'Marketing Bonus Card periodic update': {
        'task': 'marketing.tasks.get_bonus_card', 'schedule': crontab(minute='*/1'),
    },
    'Stores Store periodic update': {
        'task': 'stores.tasks.get_store', 'schedule': crontab(minute='*/1'),
    },
}
