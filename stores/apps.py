from django.apps import AppConfig


class StoresConfig(AppConfig):
    name = 'stores'
    verbose_name = 'Магазины'

    def ready(self):
        import stores.signals
