from django.test import TestCase
from stores import models


class StoresTestCase(TestCase):

    fixtures = ['fixture_stores.json']

    def setUp(self) -> None:
        self.store = models.Store.objects.last()

    def test_store_str(self):
        self.assertEqual(self.store.__str__(), 'stock')

