from libraries.base import BaseManager


class StoreManager(BaseManager):
    """
    manager for stores
    """
    URI = '/api/stores/store/'
