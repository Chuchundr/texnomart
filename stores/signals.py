"""
package: stores
description: signals
"""
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.conf import settings
from stores import models, tasks


@receiver(post_save, sender=models.Store)
def update_or_create_store_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_store.delay(instance.to_dict())


@receiver(post_delete, sender=models.Store)
def delete_store_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_store.delay(instance.to_dict())
