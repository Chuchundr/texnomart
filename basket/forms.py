"""
package: basket
description: forms
"""
from django import forms
from . import models


class BasketForm(forms.ModelForm):
    """
    form class for basket
    """
    class Meta:
        model = models.Basket
        fields = ['uuid']
        widgets = {
            'uuid': forms.HiddenInput()
        }


class BasketItemForm(forms.ModelForm):
    """
    form class for basket items
    """
    class Meta:
        model = models.BasketItem
        fields = ['good', 'number', 'basket']
        widgets = {
            'number': forms.NumberInput(attrs={'class': 'form-control'})
        }


class GoodsToPayForm(forms.ModelForm):
    """
    form for goods to pay
    """

    basket_item = forms.UUIDField()

    class Meta:
        model = models.GoodsToPay
        fields = ['user', 'good', 'number', 'for_payment']


BasketItemFormset = forms.inlineformset_factory(
    models.Basket,
    models.BasketItem,
    form=BasketItemForm,
    extra=1,
    can_delete=False
)
