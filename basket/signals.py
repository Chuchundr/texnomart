"""
package: basket
description: signals
"""
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.conf import settings
from basket import models, tasks


@receiver(post_save, sender=models.Basket)
def update_or_create_basket_hook(sender, instance, using, **kwargs):
    """
    signal for updating or creating basket
    """
    if not settings.TM_API:
        tasks.update_or_create_basket.delay(instance.to_dict())
        items = instance.basket_basketitem.all()
        for item in items:
            item.save()


@receiver(post_save, sender=models.BasketItem)
def update_or_create_basket_item_hook(sender, instance, using, **kwargs):
    """
    signal for updating or creating basket
    """
    if not settings.TM_API:
        tasks.update_or_create_basket_item.delay(instance.to_dict())


@receiver(post_delete, sender=models.Basket)
def delete_basket(sender, instance, using, **kwargs):
    """
    signal for deleting basket
    """
    if not settings.TM_API:
        tasks.delete_basket.delay(instance.to_dict())


@receiver(post_delete, sender=models.BasketItem)
def delete_basket_item_hook(sender, instance, using, **kwargs):
    """
    signal for deleting basket item
    """
    if not settings.TM_API:
        tasks.delete_basket_item.delay(instance.to_dict())
