"""
package: basket
description: urls
"""
from django.urls import path
from . import views

app_name = 'clients'

urlpatterns = [
    path('add_goods', views.BasketItemCreateView.as_view(), name='add-goods'),
    path('detail/<uuid:pk>', views.BasketDetailView.as_view(), name='basket-detail'),
    path('delete/<uuid:pk>', views.BasketItemDeleteView.as_view(), name='delete-item'),
    path('add_good_to_pay/<slug:slug>', views.GoodsToPayCreate.as_view(), name='add-goods-to-pay'),
]
