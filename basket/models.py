"""
package: basket
description: models
"""
from uuid import uuid4
from decimal import Decimal
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.core.exceptions import ObjectDoesNotExist 
from goods.models import Good


class Basket(models.Model):
    """
    model for basket
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    
    class Meta:
        verbose_name = 'Корзина'
        verbose_name_plural = 'Корзины'

    def count_total_price(self):
        total_price = Decimal()
        for item in self.basket_item.all():
            total_price += item.price
        return total_price

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid)
        }

    def get_absolute_url(self):
        return reverse('basket:basket-detail', kwargs={'uuid': str(self.uuid)})


class BasketItem(models.Model):
    """
    model for basket items
    """
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid4
    )
    basket = models.ForeignKey(
        Basket,
        on_delete=models.CASCADE,
        related_name='basket_basketitem',
        verbose_name='корзина'
    )
    good = models.OneToOneField(
        Good,
        on_delete=models.CASCADE,
        related_name='good_basketitem',
        verbose_name='товар'
    )
    number = models.PositiveIntegerField(verbose_name='количество')
    price = models.DecimalField(
        max_digits=50,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name='цена'
    )
    prepayment = models.DecimalField(
        max_digits=50,
        null=True, blank=True,
        decimal_places=2,
        verbose_name="Предоплата/Сум"
    )
    monthly = models.DecimalField(
        max_digits=50,
        decimal_places=2,
        blank=True,
        null=True,
        verbose_name="Оплата в месяц/Сум"
    )

    class Meta:
        verbose_name = 'Товар в корзине'
        verbose_name_plural = 'Товар в корзине'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'basket': str(self.basket.uuid),
            'good': str(self.good.uuid),
            'number': self.number,
            'price': str(self.price),
            'prepayment': str(self.prepayment),
            'monthly': str(self.monthly),
        }

    def recount(self):
        try:
            good = self.good
            basket = self.basket
            bonus_card = basket.user_profile.bonus_card
            percent = bonus_card.percent
            self.prepayment = Decimal(good.total_price) / 100 * percent
            price = Decimal(good.total_price) - self.prepayment
            monthly = str(price / self.good.credit_time).split('.')
            self.monthly = Decimal(f'{monthly[0]}.{monthly[1][:2]}')
        except ObjectDoesNotExist:
            self.prepayment = None
            self.monthly = None

    def save(self, *args, **kwargs):
        self.recount()
        self.price = self.good.total_price * self.number
        super(BasketItem, self).save(*args, **kwargs)


class GoodsToPay(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_goodstopay', verbose_name='пользователь')
    good = models.ForeignKey(Good, on_delete=models.CASCADE, related_name='good_goodstopay', verbose_name='товар')
    number = models.PositiveIntegerField(verbose_name='количество')
    for_payment = models.DecimalField(max_digits=50, blank=True, decimal_places=2, verbose_name='на оплату')

    def save(self, *args, **kwargs):
        self.for_payment = self.good.total_price
        super(GoodsToPay, self).save(*args, **kwargs)

    class Meta:
        verbose_name = 'товар на оплату'
        verbose_name_plural = 'товары на оплату'