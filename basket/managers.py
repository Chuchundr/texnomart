"""
package: basket
description: managers
"""
from libraries.base import BaseManager


class BasketManager(BaseManager):
    """
    manager for baskets
    """
    URI = '/api/basket/basket/'


class BasketItemManager(BaseManager):
    """
    manager for basket items
    """
    URI = '/api/basket/basket_item/'
