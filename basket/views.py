"""
package: basket
description: views
"""
from django.views import generic
from django.urls import reverse
from django.conf import settings
from django.shortcuts import redirect
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import User
from django.utils.decorators import method_decorator
from . import models, forms
from basket.models import BasketItem


@method_decorator(permission_required('basket.change_basket', login_url=settings.LOGIN_URL), name='dispatch')
class BasketItemCreateView(generic.CreateView):
    """
    view for creating basket items
    """
    model = models.BasketItem
    form_class = forms.BasketItemForm
    template_name = 'basket/basket_form.html'

    def get_success_url(self):
        return reverse('tm_site:index')


@method_decorator(permission_required('basket.view_basket', login_url=settings.LOGIN_URL), name='dispatch')
class BasketDetailView(generic.DetailView):
    """
    detail view for basket
    """
    model = models.Basket
    template_name = 'basket/basket_detail.html'

    def get_context_data(self, *args, **kwargs):
        context = super(BasketDetailView, self).get_context_data(*args, **kwargs)
        context['profile'] = self.object.user_profile
        context['user'] =self.object.user_profile.user
        return context


@method_decorator(permission_required('basket.delete_basketitem', login_url=settings.LOGIN_URL), name='dispatch')
class BasketItemDeleteView(generic.edit.DeleteView):
    """
    delete view for basket item
    """
    model = models.BasketItem
    template_name = 'basket/basket_form'

    def get_success_url(self, *args, **kwargs):
        return reverse('basket:basket-detail', kwargs={'pk': str(self.object.basket.uuid)})



class GoodsToPayCreate(generic.CreateView):
    """
    create view for goods to pay
    """
    model = models.GoodsToPay
    template_name = 'clients/goods_to_pay.html'
    form_class = forms.GoodsToPayForm

    def form_valid(self, form):
        if form.is_valid():
            form.save()
        return redirect(self.get_success_url(form.cleaned_data['user']))

    def get_success_url(self, *args, **kwargs):
        basket = User.objects.get(id=args[0].id).profile.basket
        return reverse('basket:basket-detail',
                       kwargs={'pk': basket.uuid})