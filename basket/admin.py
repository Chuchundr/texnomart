from django.contrib import admin
from . import models


class BasketItemInline(admin.TabularInline):
    """
    inline class for basket items
    """
    model = models.BasketItem


@admin.register(models.Basket)
class BasketAdmin(admin.ModelAdmin):
    """
    admin class for basket
    """
    model = models.Basket
    list_display = ['uuid']
    inlines = [BasketItemInline]


@admin.register(models.BasketItem)
class BasketItemsAdmin(admin.ModelAdmin):
    """
    admin class for basket items
    """
    model = models.BasketItem
    list_display = ['uuid', 'basket', 'good', 'number']


@admin.register(models.GoodsToPay)
class GoodsToPayAdmin(admin.ModelAdmin):
    """
    деньги на оплату
    """
    model = models.GoodsToPay
    list_display = ['user', 'good', 'number', 'for_payment']
    list_filter = ['user', 'good']