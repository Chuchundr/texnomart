"""
package: basket
description: apps
"""
from django.apps import AppConfig


class BasketConfig(AppConfig):
    name = 'basket'
    verbose_name = 'Корзины'

    def ready(self):
        import basket.signals
