=============================
Django package: Texnomart.uz
=============================

.. image:: https://badge.fury.io/py/gallery.svg
    :target: https://badge.fury.io/py/gallery

.. image:: https://travis-ci.org/flashboyka/gallery.svg?branch=master
    :target: https://travis-ci.org/flashboyka/gallery

.. image:: https://codecov.io/gh/flashboyka/gallery/branch/master/graph/badge.svg
    :target: https://codecov.io/gh/flashboyka/gallery

Texnomart.uz

Documentation
-------------

The full documentation is at https://gallery.readthedocs.io.

Quickstart
----------

Install Django texnomart.uz::

    pip install profile

Add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'address',
        'basket',
        'clients',
        'goods',
        'marketing',
        'price',
        'stores',
        'tm_site',
        ...
    )

