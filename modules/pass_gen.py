from uuid import uuid4
import random


def gen_pw():
    return f'{str(uuid4())[:5]}{random.randrange(100, 999, 1)}'
