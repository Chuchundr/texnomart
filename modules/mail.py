import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def send_mail(address_from, password, address_to, new_password):
    msg = MIMEMultipart()
    msg['From'] = address_from
    msg['To'] = address_to
    msg['Subject'] = 'Пароль'
    body = f'Ваш пароль: {new_password}'
    msg.attach(MIMEText(body, 'plain'))

    smtp = smtplib.SMTP('smtp.gmail.com', 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(address_from, password)
    smtp.send_message(msg)
    smtp.quit()