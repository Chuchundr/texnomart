"""
package: tm_site
description: urls
"""
from django.urls import path, include
from . import views

app_name = 'tm_site'

urlpatterns = [
    path('', views.MainPageView.as_view(), name='index')
]
