"""
package: tm_site
description: apps
"""
from django.apps import AppConfig


class TmSiteConfig(AppConfig):
    """
    онфигурация для приложения tm_site
    """
    name = 'tm_site'
