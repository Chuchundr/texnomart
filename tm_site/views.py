"""
package: tm_site
description: views
"""
from django.views import generic
from django.urls import reverse
from goods import models as goods_models
from basket import forms as basket_forms


class MainPageView(generic.TemplateView):
    """
    view for main page
    """
    template_name = 'index.html'

    def get_context_data(self): # pylint: disable=arguments-differ
        context = super(MainPageView, self).get_context_data()
        context['goods'] = goods_models.Good.objects.all()
        context['basket'] = basket_forms.BasketItemForm
        return context

    def get_success_url(self): # pylint: disable=no-self-use
        """
        success url
        """
        return reverse('tm_site:index')
