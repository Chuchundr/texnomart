"""
package: clients
description: forms
"""
from django import forms
from django.contrib.auth.models import User
from . import models


class PhoneNumberForm(forms.ModelForm):
    """
    форма для заполнения контактных данных
    """
    class Meta:
        model = models.Contacts
        fields = ['phone_number1', 'phone_number2', 'email']


class UserForm(forms.ModelForm):
    """
    форма для заполнения логина
    """
    class Meta:
        model = User
        fields = ['username', 'password']
        widgets = {'password': forms.TextInput(attrs={'type': 'password'})}

class PersonalDataForm(forms.ModelForm):
    """
    форма для заполнения личных данных
    """
    class Meta:
        model = models.PersonalData
        fields = ['first_name', 'last_name', 'middle_name', 'photo']
        widgets = {
            'user': forms.HiddenInput()
        }

