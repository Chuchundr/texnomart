"""
package: clients
description: views
"""
from django.views import generic
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.shortcuts import redirect
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import permission_required
from django.utils.decorators import method_decorator
from django.conf import settings
from modules import pass_gen
from . import models, forms, tasks as clients_tasks



class PhoneVerificationView(generic.CreateView):
    """
    представление для регистрации пользователя
    с указанием телефонного номера
    """
    form_class = forms.PhoneNumberForm
    template_name = 'clients/phone_verification.html'

    def get_success_url(self):
        return reverse('clients:registration',
                       kwargs={'slug': self.object.slug})


class RegisterView(generic.CreateView):
    """
    представление для продолжения регистрации, для
    указания данных пользователя
    """
    form_class = UserCreationForm
    template_name = 'clients/registration.html'

    def get_context_data(self, *args, **kwargs):  # pylint: disable=arguments-differ
        context = super(RegisterView, self).get_context_data(*args, **kwargs)
        context['personal_form'] = forms.PersonalDataForm
        return context

    def get_success_url(self, user):
        authenticate(username=user.username,
                     password=user.password)
        login(self.request, user)
        return reverse('tm_site:index')

    def form_valid(self, form):
        if form.is_valid():
            user = form.save()  # saving cleaned data into model
            personal = forms.PersonalDataForm(self.request.POST).save()  # saving cleaned data from
            # personal_data form into PersonalData model
            contacts = models.Contacts.objects.get(
                slug=self.kwargs.get('slug'))  # defining variable that is Contacts model instance
            user.profile.contacts, user.profile.personal_data = contacts, personal  # writing personal data and contacts
            # into user model
            user.profile.save()  # saving into user model
        return redirect(self.get_success_url(user))


class LoginView(generic.FormView):
    """
    представдение для авторизации
    """
    form_class = AuthenticationForm
    template_name = 'clients/login.html'

    def form_valid(self, form):
        user = form.get_user()
        login(self.request, user)
        return super(LoginView, self).form_valid(form)

    def get_success_url(self):
        return reverse('tm_site:index')


class LogoutView(generic.base.View):
    """
    представление для выхода из учетной записи
    """

    def get(self, request):
        """
        метод для логаута
        """
        logout(request)
        return HttpResponseRedirect(reverse('tm_site:index'))


@method_decorator(permission_required('clients.view_userprofile', login_url=settings.LOGIN_URL), name='dispatch')
@method_decorator(permission_required('clients.change_userprofile', login_url=settings.LOGIN_URL), name='dispatch')
class ClientDetailView(generic.DetailView):
    model = models.UserProfile
    template_name = 'clients/user_profile.html'
