"""
package: clients
description: tasks
"""
from __future__ import absolute_import, unicode_literals
import logging
from django.db import IntegrityError, OperationalError
from kombu import exceptions as kombu_exceptions
from aiohttp.client_exceptions import ClientConnectorError, ClientOSError
from django.contrib.auth.models import User
from django.core.mail import send_mail
from project.celery import app
from clients import managers, models


LOGGER = logging.getLogger(__name__)

'''
@app.task(bind=True)
def send_password(self, new_password):
    """
    task for sending pass via mail
    """
    send_mail(
        subject='Ваш пароль',
        message=new_password,
        from_email='dreamquestff@gmail.com',
        recipient_list=['farruh1607@mail.ru']
    )
'''


@app.task(bind=True)
def update_or_create_user(self, data):
    """
    task for updating or creating user
    """
    try:
        manager = managers.UserManager()
        return manager.update_or_create_user(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_group(self, data):
    """
    task for updating or creating group
    """
    try:
        manager = managers.GroupManager()
        return manager.update_or_create_group(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_user_profile(self, data):
    """
    task for updating or creating user profile
    """
    try:
        manager = managers.UserProfileManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_contacts(self, data):
    """
    task for creating or updating contacts
    """
    try:
        manager = managers.ContactsManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_personal_data(self, data):
    """
    task for creating or updating personal data
    """
    try:
        manager = managers.PersonalDataManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_contacts(self):
    """
    task for getting contacts
    """
    try:
        manager = managers.ContactsManager()
        for item in manager.get_list():
            models.Contacts.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_personal_data(self):
    try:
        manager = managers.PersonalDataManager()
        for item in manager.get_list():
            models.PersonalData.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_user_profile(self):
    try:
        manager = managers.UserProfileManager()
        for item in manager.get_list():
            models.UserProfile.objects.update_or_create(
                user_id=str(User.objects.get(username=item.pop('user')).id),
                bonus_card_id=item.pop('bonus_card'),
                contacts=item.pop('contacts'),
                personal_data=item.pop('personal_data')
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_user_profile(self, data):
    try:
        manager = managers.UserProfileManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_user(self, data):
    try:
        manager = managers.UserManager()
        return manager.delete_user(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_group(self, data):
    try:
        manager = managers.GroupManager()
        return manager.delete_group(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_contact(self, data):
    try:
        manager = managers.ContactsManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_personal_data(self, data):
    try:
        manager = managers.PersonalDataManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)
