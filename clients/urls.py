"""
package: clients
description: urls
"""
from django.urls import path
from . import views

app_name = 'clients'

urlpatterns = [
    path('registration', views.PhoneVerificationView.as_view(), name='registration-contacts'),
    path('registration/<slug:slug>', views.RegisterView.as_view(), name='registration'),
    path('login', views.LoginView.as_view(), name='login'),
    path('logout', views.LogoutView.as_view(), name='logout'),
    path('profile/<uuid:pk>', views.ClientDetailView.as_view(), name='profile'),
]
