"""
package: clients
description: signals
"""
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.conf import settings
from django.contrib.auth.models import User, Group
from clients import models, tasks
from basket.models import Basket


def to_dict(data):
    try:
        return {'username': data.username, 'password': data.password}
    except AttributeError:
        return {'name': data.name}


@receiver(post_save, sender=User)
def update_or_create_user_hook(sender, instance, using, created, **kwargs):
    """
    signal for updating or creating users
    """
    if not settings.TM_API and created:
        basket = Basket.objects.create()
        basket.save()
        tasks.update_or_create_user.delay(to_dict(instance))
        user = models.UserProfile.objects.update_or_create(user=instance, basket=basket)
        user[0].save()


@receiver(post_save, sender=Group)
def update_or_create_group_hook(sender, instance, using, **kwargs):
    """
    signal for updating or creating group
    """
    if not settings.TM_API:
        tasks.update_or_create_group.delay(to_dict(instance))


@receiver(post_delete, sender=User)
def delete_user_hook(sender, instance, using, **kwargs):
    """
    signal for deleting user
    """
    if not settings.TM_API:
        tasks.delete_user.delay(to_dict(instance))


@receiver(post_delete, sender=Group)
def delete_group_hook(sender, instance, using, **kwargs):
    """
    signal for deleting group
    """
    if not settings.TM_API:
        tasks.delete_group.delay(to_dict(instance))


@receiver(post_save, sender=models.Contacts)
def update_or_create_contacts_hook(sender, instance, using, **kwargs):
    """
    signal for creating or updating contacts
    """
    if not settings.TM_API:
        tasks.update_or_create_contacts.delay(instance.to_dict())


@receiver(post_save, sender=models.PersonalData)
def update_or_create_personal_data_hook(sender, instance, using, **kwargs):
    """
    signal for creating or updating personal data
    """
    if not settings.TM_API:
        tasks.update_or_create_personal_data.delay(instance.to_dict())


@receiver(post_save, sender=models.UserProfile)
def update_or_create_user_profile_hook(sender, instance, created, using, **kwargs):
    """
    signal for creating or updating user profile
    """
    if not settings.TM_API and instance.contacts and instance.personal_data:
        tasks.update_or_create_user_profile.delay(instance.to_dict())
        instance.basket.save()


@receiver(post_delete, sender=models.Contacts)
def delete_contact_hook(sender, instance, using, **kwargs):
    """
    signal for deleting contacts
    """
    if not settings.TM_API:
        tasks.delete_contact.delay(instance.to_dict())


@receiver(post_delete, sender=models.PersonalData)
def delete_personal_data_hook(sender, instance, using, **kwargs):
    """
    signal for deleting personal data
    """
    if not settings.TM_API:
        tasks.delete_personal_data.delay(instance.to_dict())


@receiver(post_delete, sender=models.UserProfile)
def delete_user_profile_hook(sender, instance, using, **kwargs):
    """
    signal for deleting user profile
    """
    if not settings.TM_API:
        tasks.delete_user_profile.delay(instance.to_dict())

