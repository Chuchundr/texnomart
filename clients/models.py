"""
package: clients
description: models
"""
from uuid import uuid4
from django.db import models
from django.contrib.auth.models import User, Group
from django.core.exceptions import ObjectDoesNotExist
from pytils.translit import slugify  # для перевода кирилицы
from marketing.models import BonusCard
from basket.models import Basket
from goods.models import Good

class Contacts(models.Model):
    """
    модель для контактных данных клиента
    """
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid4,
        verbose_name='идентификатор')
    phone_number1 = models.CharField(
        max_length=50,
        unique=True,
        verbose_name='Номер телефона')
    phone_number2 = models.CharField(
        max_length=50,
        blank=True,
        verbose_name='Запасной номер')
    email = models.CharField(
        max_length=100,
        verbose_name='Адрес электронной почты')
    slug = models.SlugField(
        max_length=200,
        editable=False,
        verbose_name='ЧПУ')

    def __str__(self):
        return f'{self.phone_number1} {self.email}'

    def save(self, *args, **kwargs):  # pylint: disable=arguments-differ
        self.slug = slugify(f'{self.phone_number1}')
        super(Contacts, self).save(*args, **kwargs)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'phone_number1': self.phone_number1,
            'phone_number2': self.phone_number2,
            'email': self.email,
        }

    class Meta:
        unique_together = ['phone_number1', 'email']
        verbose_name = 'контакт'
        verbose_name_plural = 'контакты'


class PersonalData(models.Model):
    """
    Модель для личных данных клиента
    """
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid4,
        verbose_name='идентификатор')
    first_name = models.CharField(max_length=100, verbose_name='Имя')
    last_name = models.CharField(max_length=100, verbose_name='Фамилия')
    middle_name = models.CharField(max_length=100, verbose_name='Отчество')
    photo = models.ImageField(blank=True, upload_to='profile-picture', default="girl.jpg", verbose_name='фото профиля')
    
    def __str__(self):
        return f'{self.first_name} {self.last_name} {self.middle_name}'

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'first_name': self.first_name,
            'last_name': self.last_name,
            'middle_name': self.middle_name,
        }

    class Meta:
        verbose_name = 'Личные данные'
        verbose_name_plural = 'личные данные'


def default_bonus_card():
    try:
        return BonusCard.objects.get(name='Member')
    except:
        return None


class UserProfile(models.Model):
    """
    модель для данных пользователя
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        related_name='profile',
        verbose_name='Пользователь')
    bonus_card = models.OneToOneField(
        BonusCard,
        on_delete=models.SET_DEFAULT,
        default=default_bonus_card,
        blank=True,
        null=True,
        verbose_name='бонусная карта')
    basket = models.OneToOneField(
        Basket,
        on_delete=models.PROTECT,
        related_name='user_profile',
        verbose_name='корзина')
    contacts = models.ForeignKey(
        Contacts,
        on_delete=models.SET_NULL,
        blank=True,
        null=True,
        verbose_name='контакты')
    personal_data = models.ForeignKey(
        PersonalData,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='личные данные')
    group = models.ForeignKey(
        Group,
        on_delete=models.SET_NULL,
        null=True,
        verbose_name='группа')
    class Meta:
        verbose_name = 'Профиль пользователя'
        verbose_name_plural = 'Профили пользователей'

    def __str__(self):
        return f'{self.user.username}{self.uuid}'

    def to_dict(self):
        if self.bonus_card:
            return {
                'uuid': str(self.uuid),
                'user': self.user.username,
                'bonus_card': str(self.bonus_card.uuid),
                'contacts': str(self.contacts.uuid),
                'personal_data': str(self.personal_data.uuid),
                'basket': str(self.basket.uuid),
                'group': self.group.name
            }
        return {
            'uuid': str(self.uuid),
            'contacts': str(self.contacts.uuid),
            'personal_data': str(self.personal_data.uuid),
            'user': self.user.username,
            'basket': str(self.basket.uuid),
            'group': self.group.name
        }

    def save(self, *args, **kwargs):
        try:
            group = Group.objects.get(name='Клиенты')
            self.user.groups.add(group)
            self.group = group
        except ObjectDoesNotExist:
            pass
        super(UserProfile, self).save(*args, **kwargs)


