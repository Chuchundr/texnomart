from libraries.base import BaseManager


class UserManager(BaseManager):
    """
    manager for users
    """
    URI = '/api/clients/user/'


class ContactsManager(BaseManager):
    """
    manager for clients
    """
    URI = '/api/clients/contacts/'


class PersonalDataManager(BaseManager):
    """
    manager for personal data
    """
    URI = '/api/clients/personal/'


class UserProfileManager(BaseManager):
    """
    manager for user profiles
    """
    URI = '/api/clients/user_profile/'


class GroupManager(BaseManager):
    """
    manager for groups
    """
    URI = '/api/clients/group/'
