"""
package: address
description: signals
"""
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.conf import settings
from address import tasks, models


@receiver(post_save, sender=models.Country)
def update_or_create_country_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for creating or updating a country
    """
    if not settings.TM_API:
        tasks.update_or_create_country.delay(instance.to_dict())


@receiver(post_save, sender=models.Region)
def update_or_create_region_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for creating or updating a region
    """
    if not settings.TM_API:
        tasks.update_or_create_region.delay(instance.to_dict())


@receiver(post_save,  sender=models.City)
def update_or_create_city_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for creating or updating a city
    """
    if not settings.TM_API:
        tasks.update_or_create_city.delay(instance.to_dict())


@receiver(post_save, sender=models.Area)
def update_or_create_area_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for creating or updating an area
    """
    if not settings.TM_API:
        tasks.update_or_create_area.delay(instance.to_dict())


@receiver(post_save, sender=models.Street)
def update_or_create_street_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for creating or updating a street
    """
    if not settings.TM_API:
        tasks.update_or_create_street.delay(instance.to_dict())


@receiver(post_save, sender=models.Address)
def update_or_create_address(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for creating or updating an address
    """
    if not settings.TM_API:
        tasks.update_or_create_address.delay(instance.to_dict())


@receiver(post_delete, sender=models.Country)
def delete_country_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for deleting a country
    """
    if not settings.TM_API:
        tasks.delete_country.delay(instance.to_dict())


@receiver(post_delete, sender=models.Region)
def delete_region_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for deleting a region
    """
    if not settings.TM_API:
        tasks.delete_region.delay(instance.to_dict())


@receiver(post_delete, sender=models.City)
def delete_city_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for deletuing a city
    """
    if not settings.TM_API:
        tasks.delete_city.delay(instance.to_dict())


@receiver(post_delete, sender=models.Area)
def delete_area_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for deleting an area
    """
    if not settings.TM_API:
        tasks.delete_area.delay(instance.to_dict())


@receiver(post_delete, sender=models.Street)
def delete_street_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for deleting a street
    """
    if not settings.TM_API:
        tasks.delete_street.delay(instance.to_dict())


@receiver(post_delete, sender=models.Address)
def delete_address_hook(sender, instance, using, **kwargs):  # pylint:disable=unused-argument
    """
    signal for deleting an address
    """
    if not settings.TM_API:
        tasks.delete_address.delay(instance.to_dict())
