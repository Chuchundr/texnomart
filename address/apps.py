"""
Module APP
"""
from django.apps import AppConfig


class AddressConfig(AppConfig):
    """
    Class with application name
    """
    name = 'address'
    verbose_name = 'адреса'

    def ready(self):
        import address.signals
