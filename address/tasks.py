"""
package: address
description: tasks
"""
from __future__ import absolute_import, unicode_literals
import logging
from address import managers
from address import models
from project.celery import app
from django.db import IntegrityError, OperationalError
from kombu import exceptions as kombu_exceptions
from aiohttp.client_exceptions import ClientConnectorError, ClientOSError


LOGGER = logging.getLogger(__name__)


@app.task(bind=True)
def get_country(self):
    try:
        manager = managers.CountryManager()
        for item in manager.get_list():
            models.Country.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_region(self):
    try:
        manager = managers.RegionManager()
        for item in manager.get_list():
            models.Region.objects.update_or_create(
                parent_id=item.pop('parent'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_city(self):
    try:
        manager = managers.CityManager()
        for item in manager.get_list():
            models.City.objects.update_or_create(
                parent_id=item.pop('parent'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_area(self):
    try:
        manager = managers.AreaManager()
        for item in manager.get_list():
            models.Area.objects.update_or_create(
                parent_id=item.pop('parent'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_street(self):
    try:
        manager = managers.StreetManager()
        for item in manager.get_list():
            models.Street.objects.update_or_create(
                parent_id=item.pop('parent'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_address(self):
    try:
        manager = managers.AddressManager()
        for item in manager.get_list():
            models.Address.objects.update_or_create(
                country_id=item.pop('country'),
                region_id=item.pop('region'),
                city_id=item.pop('city'),
                area_id=item.pop('area'),
                street_id=item.pop('street'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_country(self, data):
    try:
        manager = managers.CountryManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_region(self, data):
    try:
        manager = managers.RegionManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_city(self, data):
    try:
        manager = managers.CityManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_area(self, data):
    try:
        manager = managers.AreaManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_street(self, data):
    try:
        manager = managers.StreetManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_address(self, data):
    try:
        manager = managers.AddressManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_country(self, data):
    try:
        manager = managers.CountryManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_region(self, data):
    try:
        manager = managers.RegionManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_city(self, data):
    try:
        manager = managers.CityManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_area(self, data):
    try:
        manager = managers.AreaManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_street(self, data):
    try:
        manager = managers.StreetManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_address(self, data):
    try:
        manager = managers.AddressManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)
