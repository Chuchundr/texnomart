from libraries.base import BaseManager


class CountryManager(BaseManager):
    """
    manager for countries
    """
    URI = '/api/address/country/'


class RegionManager(BaseManager):
    """
    manager for regions
    """
    URI = '/api/address/region/'


class CityManager(BaseManager):
    """
    manager for cities
    """
    URI = '/api/address/city/'


class AreaManager(BaseManager):
    """
    manager for areas
    """
    URI = '/api/address/area/'


class StreetManager(BaseManager):
    """
    manager for streets
    """
    URI = '/api/address/street/'


class AddressManager(BaseManager):
    """
    manager for addresses
    """
    URI = '/api/address/address/'
