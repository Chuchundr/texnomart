"""
Models files for address app
"""
from uuid import uuid4
from django.db import models
from django.shortcuts import reverse
from pytils.translit import slugify


class Country(models.Model):
    """
    Model Country
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    country = models.CharField(max_length=30, unique=True, verbose_name="Страна")
    slug = models.SlugField(max_length=140, db_index=True, blank=True, editable=False, verbose_name="ЧПУ")

    class Meta:
        verbose_name = "Страна"
        verbose_name_plural = "Страны"

    '''
    def get_absolute_url(self):
        """
        Function returns url
        """
        return reverse('address:country-update', kwargs={'slug': self.slug})
    '''
    
    def __str__(self):
        return self.country

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        self.slug = slugify(self.country)
        super(Country, self).save(*args, **kwargs)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'country': self.country,
        }


class Region(models.Model):
    """
    Model Region
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    parent = models.ForeignKey(Country, on_delete=models.CASCADE, verbose_name="Страны", related_name='regions')
    region = models.CharField(max_length=30, unique=True, verbose_name="Область")
    slug = models.SlugField(max_length=140, db_index=True, blank=True, editable=False, verbose_name="ЧПУ")

    class Meta:
        verbose_name = "Область"
        verbose_name_plural = "Области"

    '''
    def get_absolute_url(self):
        """
        Function returns url
        """
        return reverse('address:region-update', kwargs={'slug': self.slug})
    '''

    def __str__(self):
        return self.region

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        self.slug = slugify(self.region)
        super(Region, self).save(*args, **kwargs)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'parent': str(self.parent.uuid),
            'region': self.region
        }


class City(models.Model):
    """
    Model City
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    parent = models.ForeignKey(Region, on_delete=models.CASCADE, verbose_name="Области", related_name='citys')
    city = models.CharField(max_length=20, unique=True, verbose_name="Город")
    slug = models.SlugField(max_length=140, db_index=True, blank=True, editable=False, verbose_name="ЧПУ")

    class Meta:
        verbose_name = "Город"
        verbose_name_plural = "Города"

    '''
    def get_absolute_url(self):
        """
        Function returns url
        """
        return reverse('address:city-update', kwargs={'slug': self.slug})
    '''

    def __str__(self):
        return self.city

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        self.slug = slugify(self.city)
        super(City, self).save(*args, **kwargs)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'parent': str(self.parent.uuid),
            'city': self.city
        }


class Area(models.Model):
    """
    Model Area
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    parent = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
        verbose_name="Города",
        related_name='areas'
    )
    area = models.CharField(max_length=30, unique=True, verbose_name="Район")
    slug = models.SlugField(max_length=140, db_index=True, blank=True, editable=False, verbose_name="ЧПУ")

    class Meta:
        verbose_name = "Район"
        verbose_name_plural = "Районы"

    '''
    def get_absolute_url(self):
        """
        Function returns url
        """
        return reverse('address:area-update', kwargs={'slug': self.slug})
    '''

    def __str__(self):
        return self.area

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        self.slug = slugify(self.area)
        super(Area, self).save(*args, **kwargs)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'parent': str(self.parent.uuid),
            'area': self.area
        }


class Street(models.Model):
    """
    Model Street
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    parent = models.ForeignKey(
        Area,
        on_delete=models.CASCADE,
        verbose_name="Районы",
        related_name='streets'
    )
    street = models.CharField(max_length=30, unique=True, verbose_name="Улица")
    slug = models.SlugField(max_length=140, db_index=True, blank=True, editable=False, verbose_name="ЧПУ")

    class Meta:
        verbose_name = "Улица"
        verbose_name_plural = "Улицы"

    def __str__(self):
        return self.street

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        self.slug = slugify(self.street)
        super(Street, self).save(*args, **kwargs)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'parent': str(self.parent.uuid),
            'street': self.street
        }


class Address(models.Model):
    """
    Model Address
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, verbose_name="Страна")
    region = models.ForeignKey(Region, on_delete=models.CASCADE, verbose_name="Область")
    city = models.ForeignKey(City, on_delete=models.CASCADE, verbose_name="Город")
    area = models.ForeignKey(Area, on_delete=models.CASCADE, verbose_name="Район")
    street = models.ForeignKey(Street, on_delete=models.CASCADE, verbose_name="Улица")
    house = models.CharField(max_length=30, verbose_name="Дом")
    flat = models.CharField(max_length=30, blank=True, null=True, verbose_name="Каб/Кв")
    index = models.CharField(max_length=30, blank=True, null=True, verbose_name="Почтовый индекс")
    slug = models.SlugField(max_length=140, editable=False, blank=True, db_index=True, verbose_name="ЧПУ")
    lat = models.FloatField(max_length=40, blank=True, null=True, verbose_name="Широта")
    lng = models.FloatField(max_length=40, blank=True, null=True, verbose_name="Долгота")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    class Meta:
        verbose_name = "Адрес"
        verbose_name_plural = "Адреса"
        unique_together = ('country', 'region', 'city', 'area', 'street', 'house', 'flat', 'index',)

    def to_dict(self):
        """
        Function return dict
        """
        return {
            'uuid': str(self.uuid),
            'country': str(self.country.uuid),
            'region': str(self.region.uuid),
            'city': str(self.city.uuid),
            'area': str(self.area.uuid),
            'street': str(self.street.uuid),
            'house': self.house,
            'flat': self.flat,
            'index': self.index,
            'lat': self.lat,
            'lng': self.lng,
        }

    def get_absolute_url(self):
        """
        Function returns url
        """
        return reverse('address:address-update', kwargs={'slug': self.slug})

    def __str__(self):
        return "{0} {1} {2} {3} {4}".format(
            self.country,
            self.city,
            self.area,
            self.street,
            self.house,
        )

    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        # latitude = Client.coordinates(self.__str__())[1]
        # langitude = Client.coordinates(self.__str__())[0]
        # self.lat = float(latitude)
        # self.lng = float(langitude)
        self.slug = slugify(self.__str__())
        super(Address, self).save(*args, **kwargs)
