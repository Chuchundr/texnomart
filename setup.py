#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import re
import sys

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup


def get_version(*file_paths):
    """Retrieves the version from agency/__init__.py"""
    filename = os.path.join(os.path.dirname(__file__), *file_paths)
    version_file = open(filename).read()
    version_match = re.search(r"^__version__ = ['\"]([^'\"]*)['\"]",
                              version_file, re.M)
    if version_match:
        return version_match.group(1)
    raise RuntimeError('Unable to find version string.')


version = get_version("project", "__init__.py")


if sys.argv[-1] == 'publish':
    try:
        import wheel
        print("Wheel version: ", wheel.__version__)
    except ImportError:
        print('Wheel library missing. Please run "pip install wheel"')
        sys.exit()
    os.system('python setup.py sdist upload')
    os.system('python setup.py bdist_wheel upload')
    sys.exit()

if sys.argv[-1] == 'tag':
    print("Tagging the version on git:")
    os.system("git tag -a %s -m 'version %s'" % (version, version))
    os.system("git push --tags")
    sys.exit()

readme = open('README.rst').read()
# history = open('HISTORY.rst').read().replace('.. :changelog:', '')

setup(
    name='texnomart.uz',
    version=version,
    description="""Texnomart.uz""",
    long_description=readme, #+ '\n\n' + history,
    author=['Dmitriy Mosin', 'Farrukh Fazildjanov'],
    author_email=['mosind313@gmail.com', 'dreamquestff@gmail.com'],
    url='https://Flashboy@bitbucket.org/Chuchundr/texnomart.uz.git',
    packages=[
        'address',
        'basket',
        'clients',
        'goods',
        'marketing',
        'price',
        'stores',
        'tm_site',
    ],
    include_package_data=True,
    install_requires=[
        'Django>=2.2.10',
        'Pillow==7.0.0',
        'pytils==0.3',
    ],
    license="MIT",
    zip_safe=False,
    keywords='texnomart.uz',
    classifiers=[
        'Development Status :: 1 - Alpha',
        'Framework :: Django :: 2.2.10',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
)
