"""
Module apps
"""
from django.apps import AppConfig


class PriceConfig(AppConfig):
    """
    Class with application name
    """
    name = 'price'
    verbose_name = 'Ценообразование'

    def ready(self):
        import price.signals
