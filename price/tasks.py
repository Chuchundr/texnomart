"""
package: marketing
description: tasks
"""
from __future__ import absolute_import, unicode_literals
import logging
from django.db import IntegrityError, OperationalError
from kombu import exceptions as kombu_exceptions
from aiohttp.client_exceptions import ClientConnectorError, ClientOSError
from project.celery import app
from price import models, managers


LOGGER = logging.getLogger(__name__)


@app.task(bind=True)
def update_or_create_credit_time(self, data):
    try:
        manager = managers.CreditTimeManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def update_or_create_pricing(self, data):
    try:
        manager = managers.PricingManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_credit_time(self):
    try:
        manager = managers.CreditTimeManager()
        for item in manager.get_list():
            models.KreditTime.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_pricing(self):
    try:
        manager = managers.PricingManager()
        for item in manager.get_list():
            models.Pricing.objects.update_or_create(
                kredit_time_id=item.pop('kredit_time'),
                **item
            )
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_credit_time(self, data):
    try:
        manager = managers.CreditTimeManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_pricing(self, data):
    try:
        manager = managers.PricingManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)
