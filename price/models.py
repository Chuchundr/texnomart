"""
Module models
"""
from uuid import uuid4
from decimal import Decimal
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from marketing import models as market_models


class KreditTime(models.Model):
    """
    Model KreditTime
    """
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='идентификатор')
    value = models.IntegerField(unique=True, verbose_name="Временной интервал")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата добавления')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирования')

    def __str__(self):
        return str(self.value)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'value': self.value
        }

    class Meta:
        verbose_name = 'Временной интервал'
        verbose_name_plural = 'Временной интервал'


class Pricing(models.Model):
    """
    Model Pricing
    """
    uuid = models.UUIDField(
        primary_key=True,
        default=uuid4,
        verbose_name='идентификатор')
    prepayment = models.DecimalField(
        max_digits=50,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name="Предоплата/Сум")
    monthly = models.DecimalField(
        max_digits=50,
        decimal_places=2,
        null=True,
        blank=True,
        verbose_name="Ежемесячно/Сум")
    created = models.DateTimeField(
        auto_now_add=True,
        editable=False,
        verbose_name='Дата добавления')
    edited = models.DateTimeField(
        auto_now=True,
        editable=False,
        verbose_name='Дата редактирования')

    def __str__(self):
        return str(f'{self.kredit_time} месяцев')

    def to_dict(self):
        """
        Function returns dict
        """
        return {
            'uuid': str(self.uuid),
            'kredit_time': self.kredit_time,
            'prepayment': self.prepayment,
            'monthly': self.monthly,
        }
    '''
    def save(self, *args, **kwargs): # pylint: disable=arguments-differ
        try:
            good = self.good.get(pricing=self)
            basket = good.good_basketitem.basket
            bonus_card = basket.user_profile.get(basket=basket).bonus_card
            percent = bonus_card.percent
            self.prepayment = Decimal(good.total_price) / 100 * percent
            price = Decimal(good.total_price) - self.prepayment
            monthly = str(price / self.kredit_time.value).split('.')
            self.monthly = Decimal(f'{monthly[0]}.{monthly[1][:2]}')
            basket.basket_basketitem.save()
        except ObjectDoesNotExist:
            self.prepayment = None
            self.monthly = None
        super(Pricing, self).save(*args, **kwargs)
    '''
    class Meta:
        verbose_name = 'Ценообразование'
        verbose_name_plural = 'Ценообразование'
