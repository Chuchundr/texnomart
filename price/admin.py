"""
Module admin
"""
from django.contrib import admin
from price import models


@admin.register(models.KreditTime)
class KreditTimeAdmin(admin.ModelAdmin):
    """
    Class admin for KreditTime model
    """
    model = models.KreditTime
    list_display = ['value']


@admin.register(models.Pricing)
class PricingAdmin(admin.ModelAdmin):
    """
    Class admin for Pricing model
    """
    list_display = ['prepayment', 'kredit_time', 'monthly']
    #search_fields = ['price']
