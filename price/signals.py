"""
package: goods
description: signals
"""
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.conf import settings
from price import models, tasks


@receiver(post_save, sender=models.KreditTime)
def update_or_create_credit_time_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_credit_time.delay(instance.to_dict())


@receiver(post_save, sender=models.Pricing)
def update_or_create_pricing_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_pricing.delay(instance.to_dict())


@receiver(post_delete, sender=models.KreditTime)
def delete_credit_time_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_credit_time.delay(instance.to_dict())


@receiver(post_delete, sender=models.Pricing)
def delete_pricing_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_pricing.delay(instance.to_dict())
