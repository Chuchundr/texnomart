from libraries.base import BaseManager


class CreditTimeManager(BaseManager):
    """
    manager for credit time
    """
    URI = '/api/price/credit_time/'


class PricingManager(BaseManager):
    """
    manager for pricing
    """
    URI = '/api/price/pricing/'
