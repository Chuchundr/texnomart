"""
package: marketing
description: signals
"""
from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete
from django.conf import settings
from marketing import models, tasks


@receiver(post_save, sender=models.BonusCard)
def update_or_create_bonus_card_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.update_or_create_bonus_card.delay(instance.to_dict())


@receiver(post_delete, sender=models.BonusCard)
def delete_bonus_card_hook(sender, instance, using, **kwargs):
    if not settings.TM_API:
        tasks.delete_bonus_card.delay(instance.to_dict())
