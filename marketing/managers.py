from libraries.base import BaseManager


class BonusCardManager(BaseManager):
    """
    manager for bonus cards
    """
    URI = '/api/marketing/bonus_card/'
