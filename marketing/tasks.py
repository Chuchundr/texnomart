"""
package: marketing
description: tasks
"""
from __future__ import absolute_import, unicode_literals
import logging
from django.db import IntegrityError, OperationalError
from kombu import exceptions as kombu_exceptions
from aiohttp.client_exceptions import ClientConnectorError, ClientOSError
from project.celery import app
from marketing import models, managers


LOGGER = logging.getLogger(__name__)


@app.task(bind=True)
def update_or_create_bonus_card(self, data):
    try:
        manager = managers.BonusCardManager()
        return manager.update_or_create(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def get_bonus_card(self):
    try:
        manager = managers.BonusCardManager()
        for item in manager.get_list():
            models.BonusCard.objects.update_or_create(**item)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)


@app.task(bind=True)
def delete_bonus_card(self, data):
    try:
        manager = managers.BonusCardManager()
        return manager.delete_item(data)
    except IntegrityError as msg:
        raise IntegrityError(msg)
    except ClientConnectorError as msg:
        raise self.retry(exc=msg)
    except ClientOSError as msg:
        raise self.retry(exc=msg)
    except kombu_exceptions.OperationalError as msg:
        raise self.retry(exc=msg)
    except OperationalError as msg:
        raise self.retry(exc=msg)
