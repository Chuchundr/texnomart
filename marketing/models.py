from uuid import uuid4
from django.db import models


class BonusCard(models.Model):
    CARD = (
        ('Member', 'Member'),
        ('Classic', 'Classic'),
        ('Silver', 'Silver'),
        ('Gold', 'Gold'),
        ('Platinum', 'Platinum'),
    )
    uuid = models.UUIDField(primary_key=True, default=uuid4, verbose_name='Идентификатор')
    name = models.CharField(max_length=8, choices=CARD, unique=True, default='Member', verbose_name="Бонусная карта")
    percent = models.PositiveSmallIntegerField(verbose_name="Процент")
    bonus = models.PositiveSmallIntegerField(verbose_name="Бонус")
    limit = models.IntegerField(verbose_name="Лимит")
    created = models.DateTimeField(auto_now_add=True, editable=False, verbose_name='Дата создания')
    edited = models.DateTimeField(auto_now=True, editable=False, verbose_name='Дата редактирование')

    def __str__(self):
        return str(self.name)

    def to_dict(self):
        """
        returns dict
        """
        return {
            'uuid': str(self.uuid),
            'name': self.name,
            'percent': self.percent,
            'bonus': self.bonus,
            'limit': self.limit
        }

    class Meta:
        verbose_name = 'Бонусную карту'
        verbose_name_plural = 'Бонусная карта'
